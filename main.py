#!/usr/bin/env python3

from PIL import Image

def grayscale(red: int, green: int, blue: int) -> int:
    """
    Convert the value of an RGB pixel to a single grayscale value,
    using the formula from Octave.
    """
    return round(0.298936*red + 0.587043*green + 0.114021*blue)

if __name__ == "__main__":
    with Image.open("fa4b429e-a88f-4934-9a4a-1db7529cfdad_l.png") as im:
        width = im.width
        height = im.height
        pixel_access = im.load()

    # Iterate over the pixels as a test
    for w in range(width):
        for h in range(height):
            print(f"At width {w}, height {h}:")
            print(f"  {pixel_access[w, h]}")
            print(f"  In grayscale: {grayscale(*pixel_access[w, h])}")

    # The values to be used in the password formula
    value_a = height
    value_b = width
